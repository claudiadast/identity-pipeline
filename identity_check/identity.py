import SDK
import os
from datetime import datetime

#Check that all req env variables are set
env_vars = ["INPUT_BUCKET", "PLINK_PREFIX", "OUTPUT_BUCKET", "PEDDY_OUTPUT_PREFIX", "BUILD", "VCF", "PED"]

if not set(env_vars).issubset(set(os.environ)):
	print "Environment not configured properly."
	raise ValueError("The required env variables are: {}".format(", ".join(env_vars)))

input_bucket = os.environ["INPUT_BUCKET"]
plink_prefix = os.environ["PLINK_PREFIX"]
output_bucket = os.environ["OUTPUT_BUCKET"]
peddy_output_prefix = os.environ["PEDDY_OUTPUT_PREFIX"]
build = os.environ["BUILD"]
vcf = os.environ["VCF"]
ped = os.environ["PED"]
in_files = [vcf, ped, "{}.bim".format(plink_prefix), "{}.bed".format(plink_prefix), "{}.fam".format(plink_prefix)]
results_files = [ "{}_pruned.bed".format(plink_prefix), "{}_pruned.bim".format(plink_prefix), "{}_pruned.fam".format(plink_prefix),
 "{}_pruned.hh".format(plink_prefix), "{}_pruned.log".format(plink_prefix), "{}_pruned.nosex".format(plink_prefix), "plink.genome", 
 "plink.fmendel", "plink.imendel", "plink.lmendel", "plink.mendel", "{}.het_check.csv".format(peddy_output_prefix), "{}.het_check.png".format(peddy_output_prefix), 
 "{}.pca_check.png".format(peddy_output_prefix), "{}.ped_check.csv".format(peddy_output_prefix), "{}.ped_check.png".format(peddy_output_prefix), 
 "{}.ped_check.rel-difference.format(peddy_output_prefix).csv", "{}.sex_check.csv".format(peddy_output_prefix), "{}.sex_check.png".format(peddy_output_prefix), "{}.vs.html".format(peddy_output_prefix)]


start_time = datetime.now()
print "IDENTITY CHECK was started at {}.".format(str(start_time))

task = SDK.Task(
	input_bucket=input_bucket,
	plink_prefix=plink_prefix,
	output_bucket=output_bucket,
	peddy_output_prefix=peddy_output_prefix,
	build=build,
	vcf=vcf,
	ped=ped,
	in_files=in_files,
	results_files=results_files)
if not set(in_files).issubset(set(os.listdir("."))):
	task.download_files()
# task.build_cmd()
task.run_cmd()
task.upload_results()
task.cleanup()

end_time = datetime.now()
print "IDENTITY CHECK ended at {}.".format(str(end_time))
total_time = end_time - start_time
print "Total time for IDENTITY CHECK was {}.".format(str(total_time))
