# Identity Pipeline

A docker container has already been made that can run Peddy and Plink (LD-Pruning, IBD, Mendel). This container is called **lindsayliang/identity_check**.

An actual Batch-based pipeline using this container is also available, but has not been thoroughly tested.



## I. Setting Up Docker Container

Follow these instructions to pull down this container on an EC2 instance and manually run the commands:

#####Installing Docker on EC2

```bash
sudo yum install -y docker
```

```bash
sudo user mod -aG docker ec2-user
```

```bash
sudo service docker start
```

#####Pull down the docker image 

```bash
sudo docker pull lindsayliang/identity_check:dev
```

#####Mount the image to EC2 volume and run interactively

```bash
sudo docker run -v /home/ec2-user/data/:/data -it lindsayliang/identity_check:dev
```

#####Now you'll be able to run the container interactively

```bash
cd /data
```



## II. Running Peddy/Plink

Here are the commands you need to run LD Pruning, IBD, Mendel, and Peddy:

#####LD Pruning

```bash
plink --bfile {prefix} --indep 50 5 2 --allow-extra-chr
```

(**note:** `prefix` here is referring to your prefix for the binary .bim, .bed, and .fam files, which can be generated on Hail - see part **III** below)

```bash
plink --bfile {prefix} --extract plink.prune.in --make-bed --out {output} --allow-extra-chr
```

##### IBD

```bash
plink --bfile {prefix} --genome --allow-extra-chr
```

(**note:** if you want to account for LD-pruning here, `prefix` in this case would be `output` from the previous step)

##### Mendel

```bash
plink --bfile {prefix} --mendel --allow-extra-chr
```

##### Peddy

If you don't already have an index (.tbi) file for your input VCF, you will need to create one via:

```bash
bgzip -c {vcf} > {vcf}.gz
```

```bash
tabix -p {vcf} {vcf}.g
```

To run Peddy, simply run:

```bash
peddy -p 4 --plot --prefix {prefix} {vcf} {ped} {--sites hg38}
```

(**note:** `prefix` in this case is just what you want the base name of the resulting files to be. `--sites hg38` should only be used if your vcf is in hg38, otherwise leave this argument out)



## III. Creating Plink binary files on Hail

Please run the following steps on your local machine.

##### Install the hail command-line

```bash
pip install -U hail
```

(**note:** you must be in a python 3 environment to use this)

##### Create a dataproc cluster 

```bash
hailctl dataproc start {cluster-name} \
    --master-machine-type n1-standard-8 \
    --num-preemptible-workers 100 \
    --num-workers 5 \
    --num-worker-local-ssds 1 \
    --zone us-east4-b
```

(**note:** `cluster-name` in this case can be anything, such as `generate-binary-files`)

##### Run hail script to generate binary files

Before running the hail script that's used to create these binary files, make sure the VCF and PED file are already stored on Google Cloud Storage.

To run the script, you can either:

a) Download and submit the script as a python script directly to the cluster 

```bash
gsutil cp gs://cos-data/export_vcf_to_plink.py .
```

```bash
hailctl dataproc submit {cluster-name} export_vcf_to_plink.py --args "{vcf} {ped} {build} {output_bucket} {output_prefix}"
```

**(note:** the `--args` are the arguments that will be passed to the hail script, each one separated by space. Example: `--args "cos-wes.gt.vcf COS_WES.fam hg38 gs://cos-data/plink/ cos-wes"`)

b) Interactively run the hail script on a Jupyter Notebook

```bash
hailctl dataproc connect {cluster-name} notebook --zone us-east4-b
```



## IV. Kicking off the Batch-ified pipeline 

All of the files for the Batch-based pipeline can be found on BitBucket. Clone into the repo via:

```bash
git clone https://claudiadast@bitbucket.org/claudiadast/identity-pipeline.git
```

```bash
cd identity-pipeline
```

To kick off the pipeline, run the following:

```bash
python identityBatchJob.py -c identity_config.yaml
```

(**note:** `identity_config.yaml` is where the user can modify their input information. Be sure to change this before kicking off the pipeline.)

Once you kick off the pipeline and no errors show in the terminal, you can monitor its progress from the AWS Batch console!