"""
CCDG-compliant SDK for common use functions in pipeline
"""
import os
import sys
import shutil
import subprocess
import boto3
import yaml
import uuid

s3 = boto3.resource("s3")

class Task:
	""" 
	A step in the pipeline.
	Attributes: 
		step -> eg bwa_mem, sort_sam...
		params -> the name of the param.json file
		prefix -> sample prefix name
		in_files -> list of filenames used as input, eg ["sample.bam, sample.bam.bai"]
		ref_files -> list of filenames used as input, eg ["human.fasta, human.dict"]
		ref_uri -> the s3 uri where the reference files are
		in_uri -> the s3 uri where the input files are located
		out_uri -> the s3 uri where the output files will be uploaded
		
	Methods:
		download_files()
		get_prog_params()
		build_cmd()
		run_cmd()
		upload_results()
		cleanup()
	"""
	def __init__(self,
				input_bucket,
				plink_prefix,
				output_bucket,
				peddy_output_prefix,
				build,
				vcf,
				ped,
				in_files,
				results_files):
		
		self.input_bucket = input_bucket
		self.plink_prefix = plink_prefix
		self.output_bucket = output_bucket
		self.peddy_output_prefix = peddy_output_prefix
		self.build = build
		self.vcf = vcf
		self.ped = ped
		self.in_files = in_files
		self.results_files = results_files
	
	def download_files(self):
		"""
		Helper step just for genotyper - builds input_files
		from the sample_file.
		"""
		files_to_download = []
		bucket_name = self.input_bucket.split("/")[2]
		for file in self.in_files:
			key = "{}{}".format(prefix, file)
			files_to_download.append(key)
		sys.stdout.flush()
		b = s3.Bucket(bucket_name)
		for f,k in zip(files, files_to_download):
			print("Downloading {} from bucket {}.".format(k,b))
			b.download_file(k, f)

	def build_cmd(self):
		"""
		Formats the unformatted bioinformatic too command 
		and returns the command as a list of strings for
		subprocess to run
		:return cmd_list: List
		"""

		# LD-Pruning
		ld_pruning_cmd_1 = "plink --bfile {} --indep 50 5 2 --allow-extra-chr".format(self.plink_prefix)
		ld_pruning_cmd_2 = "plink --bfile {} --extract plink.prune.in --make-bed --out {}_pruned --allow-extra-chr".format(self.plink_prefix, self.plink_prefix)

		# IBD
		ibd_cmd = "plink --bfile {}_pruned --genome --allow-extra-chr".format(self.plink_prefix)

		# Mendel
		mendel_cmd = "plink --bfile {} --mendel --allow-extra-chr".format(self.plink_prefix)

		# Peddy
		if not self.vcf.endswith(".gz"):
			zip_cmd = "bgzip -c {} > {}.gz".format(self.vcf, self.vcf)
			tabix_cmd = "tabix -p vcf {}.gz".format(self.vcf)
			if self.build == "hg38":
				peddy_cmd = "peddy -p 4 --plot --prefix {} {}.gz {} --sites hg38".format(self.peddy_output_prefix, self.vcf, self.ped)
			else:
				peddy_cmd = peddy_cmd = "peddy -p 4 --plot --prefix {} {}.gz {}".format(self.peddy_output_prefix, self.vcf, self.ped)
		else:
			zip_cmd = None
			tabix_cmd = None
			if self.build == "hg38":
				peddy_cmd = "peddy -p 4 --plot --prefix {} {}.gz {} --sites hg38".format(self.peddy_output_prefix, self.vcf, self.ped)
			else:
				peddy_cmd = peddy_cmd = "peddy -p 4 --plot --prefix {} {}.gz {}".format(self.peddy_output_prefix, self.vcf, self.ped)

		cmd_list = [ld_pruning_cmd_1, ld_pruning_cmd_2, ibd_cmd, mendel_cmd, zip_cmd, tabix_cmd, peddy_cmd]
		sys.stdout.flush()
		return cmd_list
	
	def run_cmd(self):
		"""
		Runs the bioinformatics tool for the current step of the pipeline.
		"""
		cmd_list = self.build_cmd()

		for cmd in cmd_list:
			if cmd != None:
				return_code = subprocess.call(cmd)
				if return_code > 0:
					print("NON ZERO EXIT: {}".format(str(return_code)))
					sys.stdout.flush()
					exit(return_code)
		
	def upload_results(self):
		"""
		Uploads any output files to out_uri.
		"""
		bucket_name = self.output_bucket.split("/")[2]
		prefix = "/".join(self.output_bucket.split("/")[3:])
		for file_name in self.result_files:
			key = "{}{}".format(prefix, file_name)
			print "Uploading {} to bucket {}, key {}.".format(file_name, bucket_name, key)
			b = s3.Bucket(bucket_name)
			b.upload_file(file_name, key)
		sys.stdout.flush()

	def cleanup(self):
		"""
		Removes input, output, intermediate, and license files from cwd.
		Removes the tmp directory if a java program was used.
		"""
		print "Cleaning up instance..."
		for file in self.result_files:
			print "Removing result file {}.".format(file)
			os.remove(file)
		for file in self.in_files:
			print "Removing input file {}".format(file)
			os.remove(file)
		sys.stdout.flush()
