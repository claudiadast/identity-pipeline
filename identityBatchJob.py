'''
Makes a compute environment, job defs, etc to submit identity jobs to Batch.

'''

import boto3
import yaml
import time
import cmd
import sys
import os
import datetime
import uuid
import json
import sqlite3
import string
from sqlitedict import SqliteDict
from pprint import pprint
from botocore.exceptions import ClientError
from threading import Thread
from argparse import ArgumentParser
from batch import BatchJobListStatusPoller
from argparse import ArgumentParser


RUN_TIME = str(datetime.datetime.now())

# Establish AWS clients

batch_client = boto3.client('batch')

s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

sandbox_s3_resource = boto3.resource('s3')

sandbox_s3_client = boto3.client('s3')

asg_client = boto3.client('autoscaling')
ec2_client = boto3.client('ec2')

db_file = './trials.db.sqlite'

def unique_trial_name(base='identity-batch-trial'):
	return '-'.join((base, str(uuid.uuid4())[:4]))

instance_cost_per_hour = {
	'm4.large': (0.1, 2),
	'm4.xlarge': (0.2, 4),
	'm4.2xlarge': (0.4, 8),
	'm4.4xlarge': (0.8, 16),
	'm4.10xlarge': (2, 40),
	'm4.16xlarge': (3.2, 64),
	'c4.large': (0.1, 2),
	'c4.xlarge': (0.19, 4),
	'c4.2xlarge': (0.40, 8),
	'c4.4xlarge': (0.80, 16),
	'c4.8xlarge': (1.6, 36),
	'r4.large': (0.13, 2),
	'r4.xlarge': (0.27, 4),
	'r4.2xlarge': (0.53, 8),
	'r4.4xlarge': (1.1, 16),
	'r4.8xlarge': (2.1, 32),
	'r4.16xlarge': (4.3, 64)
}

def submit_identity_check(trial_resource_configs, user_config):

	for trial_config in trial_resource_configs:
		trial = trial_config['trial']
		q = trial_config['queue']['name'] + "_" + "_".join([str(i) for i in trial_config['trial']['instance_types']]).replace(".", "_")

	identity_def = defJob("identity_check", 
		"249640142434.dkr.ecr.us-east-1.amazonaws.com/identity-pipeline:latest",
		16, 120000, ["python", "/identity_check/identity.py"],
		"/identity_check/localDir", "/mnt/data/localDir", 3)


	identity_job = submitJob(
		name="identity_check",
		queue=q,
		jobDef="identity_check",
		containerOverrides={"environment": [
								{"name": "INPUT_BUCKET", "value": user_config["input_bucket"]},
								{"name": "PLINK_PREFIX", "value": user_config["plink_prefix"]},
								{"name": "OUTPUT_BUCKET", "value": user_config["output_bucket"]},
								{"name": "PEDDY_OUTPUT_PREFIX", "value": user_config["peddy_output_prefix"]},
								{"name": "BUILD", "value": user_config["build"]},
								{"name": "VCF", "value": user_config["vcf"]},
								{"name": "PED", "value": user_config["ped"]}]})


##########################################################################################################################


def wait_on_batch_resources_created(boto3_batch_fn, kw_args, response_key):
	success_set = set([('ENABLED', 'VALID')])
	creating_set = set(['CREATING'])
	creating_and_valid_set = set(['CREATING', 'VALID'])

	print(success_set)
	print(creating_set)
	print(creating_and_valid_set)

	while True:
		batch_response = boto3_batch_fn(**kw_args)

		print(batch_response)

		states_statuses = [(resource_config['state'], resource_config['status']) for resource_config in batch_response[response_key]]
		print(states_statuses)
		states_statuses_set = set(states_statuses)

		status_set = set([state_status[1] for state_status in states_statuses])

		if states_statuses_set == success_set:
			return True
		elif status_set == creating_set or status_set == creating_and_valid_set:
			time.sleep(1)
		else:
			print(status_set)
			print(states_statuses)
			raise Exception("Failed to create CE!")

def s3_obj_exists(bucket, key):
	try:
	    s3.Object(bucket, key).load()
	except botocore.exceptions.ClientError as e:
	    if e.response['Error']['Code'] == "404":
	        # The object does not exist.
	        return False
	    else:
	        print(e)
	else:
		return True

class AutoScalingGroupPoller(Thread):
	def __init__(self, asg_prefix):
		Thread.__init__(self)
		self.asg_prefix = asg_prefix
		# self.instances is a dict which contains instance_type : {instance_1 : (start, end), instance_2 : (start, end)}
		self.instances = {}
	
	def costOfAsg(self, costDict):
		print("****CALCULATING COST - INSTANCE TIMES:")
		pprint(self.instances)
		total_cost = 0
		instance_active_time = {}
		for instance_type in self.instances:
			instance_type_time = 0
			for instance in self.instances[instance_type]:
				instance_type_time += self.instances[instance_type][instance][1] - self.instances[instance_type][instance][0]
			# don't need to know how many instances of this type, only how many total hours
			# note: the instance_type_time is in seconds
			total_cost += costDict[instance_type][0] * (instance_type_time / 3600)
			print("TOTAL COST:", total_cost)
		return total_cost

	def run(self):
		asg_alive = False
		trial_asg = None
		while not asg_alive:
			all_asgs = asg_client.describe_auto_scaling_groups()
			for asg in all_asgs['AutoScalingGroups']:
				if asg['AutoScalingGroupName'].startswith(self.asg_prefix) and len(asg['Instances']) > 0:
					trial_asg_name = asg["AutoScalingGroupName"]
					asg_alive = True
			# Hit max retries: 4 for DescribeAutoScallingGroups - add sleep to reduce retries?
			time.sleep(5)

		while asg_alive:
			trial_asg = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[trial_asg_name])['AutoScalingGroups'][0]
			if trial_asg['Instances'] != []:
				# Get only instance ids
				trial_instance_ids = [instance_description['InstanceId'] for instance_description in trial_asg['Instances']]
				for instance_description in trial_asg['Instances']:
					instance_id = instance_description['InstanceId']
					instance_type_description = ec2_client.describe_instance_attribute(Attribute='instanceType', InstanceId=instance_id)
					instance_type = instance_type_description["InstanceType"]["Value"]
					if instance_type not in self.instances:
						# get time when instance was spun into asg
						start = time.time()
						self.instances[instance_type] = {instance_id : (start, 0)}
					elif instance_id not in self.instances[instance_type]:
						self.instances[instance_type][instance_id] = (start, 0)
				# can't assume that all instaces are in asg until asg dies, need to check if all are stil alive
				for instance_type in self.instances:
					for instance_in_asg in self.instances[instance_type]:
						if instance_in_asg not in trial_instance_ids:
							# an instance we've been keeping track of has scaled out
							end_time = time.time()
							self.instances[instance_type][instance_in_asg][1] = end_time

			else:
				# Need some way of detecting when all instances in asg are scaled out
				# This if will only be entered if asg_alive is True, aka when at least
				# one instance had been previously scaled in
				print("NO INSTANCES IN ASG")
				asg_alive = False
				# Get end time of asg for instance time lengths
			time.sleep(5)
		total_cost = self.costOfAsg(instance_cost_per_hour)

		 #Write cost analysis for all instances per trial to a file

		instance_cost_analysis = self.asg_prefix + "_instance_cost_analysis.json"
		for instance_type in self.instances:
			self.instances[instance_type] = list(self.instances[instance_type]) 
		
		with open(instance_cost_analysis, 'w') as f_writable:
			self.instances['queue_total_cost'] = total_cost
			json.dump(self.instances, f_writable)

		pprint(self.instances)

def createCompEnv(name, instances, minCPU, desiredvCpus, maxCPU, ami):
	try:
		response = batch_client.create_compute_environment(
			computeEnvironmentName=name,
			type="MANAGED",
			state="ENABLED",
			computeResources={
				"type" : "EC2",
				"desiredvCpus" : desiredvCpus,
				"minvCpus" : minCPU,
				"maxvCpus" : maxCPU,
				"instanceTypes" : instances,
				"imageId" : ami,
				"subnets" : [
					"subnet-78c18974", 
					"subnet-45f80d7a", 
					"subnet-1bafbf53",
					"subnet-85df88df",
					"subnet-a2885cc6",
					"subnet-f46538d8"],
				"securityGroupIds" : [
					"sg-5779dc22"],
				"ec2KeyPair" : "Claudia_Sandbox",
				"instanceRole" : "arn:aws:iam::249640142434:instance-profile/ec2ContainerHolding",
			},
			serviceRole="arn:aws:iam::249640142434:role/service-role/AWSBatchServiceRole"
		)
		# time.sleep(40)
		return response
	except ClientError as e:
		if e.response['Error']['Message'] == "Object already exists":
			print("[ WARN ] Compute environment " + name + " already created!")
			pass

def createQ(name, compEnv, priority):
	try:
		response = batch_client.create_job_queue(
			jobQueueName=name,
			state="ENABLED",
			priority=priority,
			computeEnvironmentOrder=[
				# Can have multipe compute environments - scheduler
				# attempts to schedule from order 1, 2,...n
				{
					"order" : 1,
					"computeEnvironment" : compEnv
				}
			]
		)
		# time.sleep(30)
		return response
	except ClientError as e:
		if e.response['Error']['Message'] == "Object already exists":
			print("[ WARN ] Queue " + name + " already created!")

def defJob(name, container, vcpus, mem, cmd, containerVol, ec2Mount, attempts):
	try:
		response = batch_client.register_job_definition(
			jobDefinitionName=name,
			type="container",
			containerProperties={
				"image" : container,
				"vcpus" : vcpus,
				"memory" : mem,
				"command" : cmd,
				"jobRoleArn" : "arn:aws:iam::249640142434:role/AmazonEC2ContainerServiceTaskRole",
				"volumes" : [
					{
						"host" : {
							"sourcePath" : ec2Mount
						},
						"name" : "localDir"
					}],
				"mountPoints" : [
					{
						"containerPath" : containerVol,
						"readOnly" : False,
						"sourceVolume" : "localDir",
					}],
				"readonlyRootFilesystem" : False,
				'ulimits': [
			            {
			                'hardLimit': 90000,
			                'name': 'nofile',
			                'softLimit': 90000
			            },
			        ],
				"privileged" : False 
			},
			retryStrategy={
				"attempts" : attempts
			}
		)
		return response
	except ClientError as e:
		if e.response['Error']['Message'] == "Object already exists":
			print ("[ WARN ] Job definition " + name + " already created!")
			pass

def submitJob(name, queue, jobDef, dependsOn=False, containerOverrides=None):
	if dependsOn and not containerOverrides:
		return batch_client.submit_job(
		jobName=name,
		jobQueue=queue,
		dependsOn=dependsOn,
		jobDefinition=jobDef)
	elif not dependsOn and not containerOverrides:
		return batch_client.submit_job(
		jobName=name,
		jobQueue=queue,
		jobDefinition=jobDef)
	elif containerOverrides and not dependsOn:
		return batch_client.submit_job(
		jobName=name,
		jobQueue=queue,
		jobDefinition=jobDef,
		containerOverrides = containerOverrides)
	elif containerOverrides and dependsOn:
		return batch_client.submit_job(
		jobName=name,
		jobQueue=queue,
		dependsOn=dependsOn,
		jobDefinition=jobDef,
		containerOverrides = containerOverrides)

def create_resources(user_config, do_create_job_def=False):
	"""
	[
		{
			'trial': trial,
			'compute_environment': {
				'name': ...
			},
			'queue': {
				'name': ...,
				'arn': ...
			}
		}
	}
	"""

		# TODO: wait on job_def create...
		# But docs don't show what the job_def_status values are...

	trial_names = [trial["trial_name"] for trial in user_config["trials"]]
	compute_environment_names = ['-'.join((trial_name, 'ce')) for trial_name in trial_names]
	queue_names = ['-'.join((trial_name, 'q')) for trial_name in trial_names]

	trial_resource_configs = []
	for trials_names in zip(user_config['trials'], compute_environment_names, queue_names):
		trial = trials_names[0]
		compute_environment_name = trials_names[1]
		queue_name = trials_names[2]

		trial_resource_configs.append(
			{
				'trial': trial,
				'compute_environment': {
					'name': compute_environment_name
				},
				'queue': {
					'name': queue_name,
					'arn': None
				}
			}
		)

	if do_create_job_def:
		for config in trial_resource_configs:
			trial = config['trial']
			print('Creating job def...')
			job_def_name = "identity_check_{}".format(trial["trial_name"])
			trial['job_def'] = job_def_name
			response = defJob(
				job_def_name,
				user_config["container"],
				trial["job_specs"]["vcpus"],
				trial["job_specs"]["mem"],
				["bash", "/identity_check/identity_setup.sh"],
				"/identity_check/localDir",
				"/mnt/data/localDir",
				2
			)
		pprint(response)

	some_create_failed = False

	creating_ces = []
	for trial_config in trial_resource_configs:
		pprint(trial_config)
		trial = trial_config['trial']
		pprint(trial)
		print('Creating resources for trial {}...'.format(trial['trial_name']))
		print('Creating compute env...')
		response = createCompEnv(
			name=trial_config['compute_environment']['name'],
			instances=trial['instance_types'],
			minCPU=trial['vcpu_specs'][0],
			desiredvCpus=trial['vcpu_specs'][1],
			maxCPU=trial['vcpu_specs'][2],
			# ami='ami-a2dd82d8'
			ami=trial['ami']
		)
		if response:
			pprint(response)
			creating_ces.append(trial_config['compute_environment']['name'])
		else:
			some_create_failed = True
			print('Failed to create ce.')

	print('Waiting on compute envs...')
	wait_on_batch_resources_created(
		boto3_batch_fn=batch_client.describe_compute_environments,
		kw_args={'computeEnvironments': creating_ces},
		response_key='computeEnvironments'
	)

	creating_qs = []
	for trial_config in trial_resource_configs:
		print('Creating Qs...')
		# Note: queue name can't have "." in it
		response = createQ(
			name=trial_config['queue']['name'] + "_" + "_".join([str(i) for i in trial_config['trial']['instance_types']]).replace(".", "_"),
			compEnv=trial_config['compute_environment']['name'],
			priority=10
		)
		if response:
			pprint(response)
			trial_config['queue']['arn'] = response['jobQueueArn']
			creating_qs.append(response['jobQueueArn'])
		else:
			some_create_failed = True
			print('Failed to create q.')

	print('Waiting on Qs...')
	wait_on_batch_resources_created(
		boto3_batch_fn=batch_client.describe_job_queues,
		kw_args={'jobQueues': creating_qs},
		response_key='jobQueues'
	)

	return some_create_failed, trial_resource_configs

class DeleteResourceLoop(cmd.Cmd):

	def __init__(self, db_file):
		super().__init__()
		self.db_file = db_file

	def key_list(self, run_dict):
		keys = list(run_dict.keys())
		keys.sort()
		return keys

	def do_l(self, line):
		with SqliteDict(db_file) as run_dict:
			keys = self.key_list(run_dict)
			for i, run_key in enumerate(keys):
				print("{}: {}".format(i, run_key))

	def do_d(self, number):
		number = int(number)
		with SqliteDict(db_file) as run_dict:
			num_runs = len(run_dict)
			if number > -1 and number < num_runs:
				keys = self.key_list(run_dict)
				key_to_describe = keys[number]
				trial_resource_configs = run_dict[key_to_describe]
				self.describe_resources(trial_resource_configs)

	def describe_resources(self, trial_configs):
		for trial_config in trial_configs:
			queue_arn = trial_config['queue']['arn']
			queue_name = trial_config['queue']['name']
			ce_name = trial_config['compute_environment']['name']

			describe_ce_response = batch_client.describe_compute_environments(computeEnvironments=[ce_name])
			if len(describe_ce_response['computeEnvironments']) > 0:
				print("{}: ({},{})".format(
					ce_name,
					describe_ce_response['computeEnvironments'][0]['status'],
					describe_ce_response['computeEnvironments'][0]['state']
				))
			else:
				print("{}: Not found".format(ce_name))

			describe_queue_response = batch_client.describe_job_queues(jobQueues=[queue_arn])
			if len(describe_queue_response['jobQueues']) > 0:
				print("{}: ({},{})".format(
					queue_name,
					describe_queue_response['jobQueues'][0]['status'],
					describe_queue_response['jobQueues'][0]['state']
				))
			else:
				print("{}: Not found".format(queue_name))

	def do_del(self, number):
		number = int(number)
		if number > -1:
			with SqliteDict(db_file) as run_dict:
				num_runs = len(run_dict)
				if number > -1 and number < num_runs:
					keys = list(run_dict.keys())
					keys.sort()
					key_to_describe = keys[number]
					trial_resource_configs = run_dict[key_to_describe]
					print('Deleting...')
					self.delete_resources(trial_resource_configs)

	def delete_resources(self, trial_configs):
		for trial_config in trial_configs:
			queue_arn = trial_config['queue']['arn']
			queue_name = trial_config['queue']['name']
			ce_name = trial_config['compute_environment']['name']

			print('Disabling q: {}'.format(queue_name))

			# first check if exists...
			response = batch_client.describe_job_queues(
				jobQueues=[queue_arn]
			)
			alter_q = False
			if response:
				if len(response['jobQueues']) > 0:
					status = response['jobQueues'][0]['status']
					alter_q = status == 'VALID' or status == 'INVALID'

			if alter_q:
				response = batch_client.update_job_queue(
					jobQueue=queue_arn,
					state='DISABLED'
				)

				if response:
					disabled = False
					while not disabled:
						response = batch_client.describe_job_queues(
							jobQueues=[queue_arn]
						)
						if response:
							#pprint(response)
							if len(response['jobQueues']) > 0:
								if response['jobQueues'][0]['state'] == 'DISABLED' and response['jobQueues'][0]['status'] == 'VALID':
									disabled = True
						time.sleep(1)

				print('Deleting q: {}'.format(queue_name))
				response = batch_client.delete_job_queue(
					jobQueue=queue_arn
				)

				if response:
					deleted = False
					while not deleted:
						response = batch_client.describe_job_queues(
							jobQueues=[queue_arn]
						)
						if response:
							#pprint(response)
							if len(response['jobQueues']) > 0:
								if response['jobQueues'][0]['status'] == 'DELETED':
									deleted = True
							else:
								deleted = True
						time.sleep(1)

			print('Deleting: {}'.format(ce_name))
			print('Disabling q: {}'.format(queue_name))

			# first check if exists...
			response = batch_client.describe_compute_environments(
				computeEnvironments=[ce_name]
			)
			alter_ce = False
			if response:
				if len(response['computeEnvironments']) > 0:
					# only alter if it is stationary
					status = response['computeEnvironments'][0]['status']
					alter_ce = status == 'VALID' or status == 'INVALID'

			if alter_ce:
				response = batch_client.update_compute_environment(
					computeEnvironment=ce_name,
					state='DISABLED'
				)

				if response:
					disabled = False
					while not disabled:
						response = batch_client.describe_compute_environments(
							computeEnvironments=[ce_name]
						)
						if response:
							#pprint(response)
							if len(response['computeEnvironments']) > 0:
								if response['computeEnvironments'][0]['state'] == 'DISABLED' and response['computeEnvironments'][0]['status'] == 'VALID':
									disabled = True
						time.sleep(1)

				print('Deleting ce: {}'.format(ce_name))
				response = batch_client.delete_compute_environment(
					computeEnvironment=ce_name
				)

				if response:
					deleted = False
					while not deleted:
						response = batch_client.describe_compute_environments(
							computeEnvironments=[ce_name]
						)
						if response:
							#pprint(response)
							if len(response['computeEnvironments']) > 0:
								if response['computeEnvironments'][0]['status'] == 'DELETED':
									deleted = True
							else:
								deleted = True
						time.sleep(1)

	def do_EOF(self, line):
		return True

def delete_resources_manager():
	DeleteResourceLoop(db_file=db_file).cmdloop()

def persist_resource_configs(db_file, trial_resource_configs):
	"""
	{
		'<run_date>': trial_resource_config
	}
	Args:
	    trial_resource_configs:

	Returns:

	"""
	now = datetime.datetime.now()
	run_key = 'Run {}-{}-{} {}:{}'.format(
		now.year,
		now.month,
		now.day,
		now.hour,
		now.minute
	)
	with SqliteDict(db_file) as run_dict:
		run_dict[run_key] = trial_resource_configs
		run_dict.commit(blocking=True)

def get_configuration(config):
	with open(config, "r") as y:
		config_dict = yaml.load(y)
		pprint(config_dict)
		for trial in config_dict['trials']:
			trial['trial_name'] = unique_trial_name()
	return config_dict

def parse_args():
	parser = ArgumentParser()
	parser.add_argument('-c', '--config', type=str, required=True)
	parser.add_argument('-m', '--manager', action='store_true', required=False)
	parser.add_argument('-j', '--register-job-def', action='store_true', required=False)

	args = parser.parse_args()
	return args.manager, args.register_job_def, args.config

def main_profiler(do_manager, register_job_def, config):

	if do_manager:
		delete_resources_manager()
		sys.exit(0)
	else:
		configuration = get_configuration(config)
		print('Creating Resources...')
		err, trial_resource_configs = create_resources(configuration, register_job_def)

		print('Persisting...')
		persist_resource_configs(
			db_file=db_file,
			trial_resource_configs=trial_resource_configs
		)

		if not err:
			print('Submitting jobs...')
			submit_identity_check(trial_resource_configs, configuration)

if __name__ == '__main__':
	do_manager, register_job_def, config = parse_args()
	main_profiler(
		do_manager,
		register_job_def,
		config
	)
